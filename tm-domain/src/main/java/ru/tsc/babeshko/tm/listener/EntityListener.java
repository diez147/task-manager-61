package ru.tsc.babeshko.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.event.OperationEvent;
import ru.tsc.babeshko.tm.enumerated.EntityOperationType;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import java.util.function.Consumer;

import static ru.tsc.babeshko.tm.enumerated.EntityOperationType.*;


@NoArgsConstructor
public final class EntityListener {

    @Nullable
    private static Consumer<OperationEvent> CONSUMER = null;

    public static Consumer<OperationEvent> getConsumer() {
        return CONSUMER;
    }

    public static void setConsumer(@NotNull final Consumer<OperationEvent> consumer) {
        EntityListener.CONSUMER = consumer;
    }

    @PostLoad
    private void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PostPersist
    private void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PostRemove
    private void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PostUpdate
    private void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        if (CONSUMER == null) return;
        CONSUMER.accept(new OperationEvent(operationType, entity));
    }

}

